# Changelog

## 4.0.0 - 2024-01-21

### Fixed

- #8 Éviter d’appeler des fonctions dépréciées.
 
### Changed

- Nécessite SPIP 4.2 minimum
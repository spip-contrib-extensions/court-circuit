# Court-circuit

Permet de rediriger certaines rubriques vers un article particulier. Vous pouvez configurer, dans l’espace privé, les règles de redirection à appliquer

## Documentation

https://contrib.spip.net/3831
<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-courtcircuit?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// C
	'courtcircuit_description' => 'Allows to redirect certain sections to a specific article. You can configure in the private area, the redirection rules to apply.',
	'courtcircuit_nom' => 'Short-circuit',
	'courtcircuit_slogan' => 'Direct access to articles',
];

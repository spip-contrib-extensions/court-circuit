<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-courtcircuit?lang_cible=es
// ** ne pas modifier le fichier **

return [

	// C
	'courtcircuit_description' => 'Permite redirigir algunas secciones hacia un artículo particular. Puede configurar, en el espacio privado, las reglas de redirección a aplicar. ',
	'courtcircuit_nom' => 'Cortocircuito',
	'courtcircuit_slogan' => 'Acceso directo a los artículos',
];

<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-courtcircuit?lang_cible=pt_br
// ** ne pas modifier le fichier **

return [

	// C
	'courtcircuit_description' => 'Permite redirecionar certas seções para uma matéria específica. Você pode configurar, na área restrita, as regras de redirecioniamento a serem usadas.',
	'courtcircuit_nom' => 'Curto-circuito',
	'courtcircuit_slogan' => 'Acesso direto a matérias',
];
